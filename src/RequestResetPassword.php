<?php

namespace KDA\Laravel\Authentication\Livewire;

use KDA\Laravel\Authentication\Facades\AuthManager;
use DanHarrin\LivewireRateLimiting\WithRateLimiting;
use DanHarrin\LivewireRateLimiting\Exceptions\TooManyRequestsException;
use Illuminate\Validation\ValidationException;

class RequestResetPassword extends Component
{
    use WithRateLimiting;
    protected $throttle_message = "Attendez %d secondes pour rééssayer";
    public $form;
    public $reset = false;
    protected $rules = [
        'form.email' => 'required|email',
    ];
    protected $messages = [
        'form.email.email' => 'L\'Adresse email n\'est pas valide',
    ];

    

    public function send()
    {
        //dump($this->form);
        try {
            $this->rateLimit(5);
        } catch (TooManyRequestsException $exception) {
            throw ValidationException::withMessages([
                'form.email' => sprintf($this->throttle_message,$exception->secondsUntilAvailable),
            ]);
        }
        $this->validate();
        return AuthManager::flowKey(static::$flow_key)->livewire(true)
            ->resetPasswordAlwaysSucceed(true)
            ->resetPasswordMessage('Si vous êtes inscrits, vous recevrez un e-mail permettant de recevoir un mot de passe')
            ->createRequest($this->form)
            ->sendResetPasswordLink()
            ->getSendResetLinkResponse();
    }

    public function render()
    {
        return view('espace-terroir-frontend::livewire.request-reset-password');
    }
}
