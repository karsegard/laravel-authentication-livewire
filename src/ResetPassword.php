<?php

namespace KDA\Laravel\Authentication\Livewire;

use KDA\Laravel\Authentication\Facades\AuthManager;

class ResetPassword extends Component
{
    public $form;
    public $token ;
    public $email ;
    protected $rules = [
        'form.email'=>'required|email',
        'form.password' => 'required|confirmed| min:6',
        'form.token'=>'required|string'
    ];

    public function mount(){
        $this->form['token'] = request()->get('token');
        $this->form['email'] = request()->get('email');
    }
    

    public function send()
    {
        $this->validate();
        $response = AuthManager::flowKey(static::$flow_key)
        ->createRequest($this->form)
        ->attemptResetPassword()
        ->getResetPasswordResponse();
        return $response;
    }

}
