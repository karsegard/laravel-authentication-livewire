<?php

namespace KDA\Laravel\Authentication\Livewire;

use EspaceTerroir\Customers\Models\Customer;
use KDA\Laravel\Authentication\Facades\AuthManager;

class ChangePassword extends Component
{
    public $form;
    protected $rules = [
        'form.password' => 'string|required',
        'form.new_password' => 'string|required|confirmed|min:6'
    ];

   

    public function save()
    {
        $this->validate();
        $this->form['email']= et_customer()->email;
        try {
            AuthManager::flowKey(static::$flow_key)
                ->livewireChangePassword($this->form);
            $this->form = [];
            session()->flash('status', 'Votre mot de passe a été mis à jour');
        } catch (\Exception $e) {
            session()->flash('error', $e->getMessage());
        }
    }
}
