<?php

namespace KDA\Laravel\Authentication\Livewire;


use KDA\Laravel\Authentication\Facades\AuthManager;

abstract class RegisterForm extends Component
{
    public $form;
    public $registered = false;
    protected $rules = [
        'form.firstname' => 'required|string',
        'form.lastname' => 'required|string',
        'form.email' => 'required|email|confirmed|unique',
        'form.password' => 'required|confirmed',
    ];

    public function send()
    {
        $this->validate();
        AuthManager::flowKey(static::$flow_key)->livewireRegister($this->form)->autologin();
        $this->registered= true;
    }
    
}
