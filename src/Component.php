<?php

namespace KDA\Laravel\Authentication\Livewire;

use KDA\Laravel\Authentication\Facades\AuthManager;
use Livewire\Component as LivewireComponent;

class Component extends LivewireComponent
{
    protected static string $view; 
    protected static string $flow_key;
    
    public function render()
    {
        return view(static::$view);
    }
}
