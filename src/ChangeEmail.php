<?php

namespace KDA\Laravel\Authentication\Livewire;

use EspaceTerroir\Customers\Models\Customer;
use KDA\Laravel\Authentication\Facades\AuthManager;

class ChangeEmail extends Component
{
    public $form ;

    protected $rules = [
        'form.new_email' => 'required|email|confirmed|unique:customers,email',
        'form.password'=>'required|string'
    ];

    public function save()
    {
        $this->validate();
        $this->form['email']= et_customer()->email;

        AuthManager::flowKey(static::$flow_key)->livewireChangeEmail($this->form);
        session()->flash('status', 'Votre e-mail a été mis à jour, veuillez valider votre adresse e-mail');
    }
}